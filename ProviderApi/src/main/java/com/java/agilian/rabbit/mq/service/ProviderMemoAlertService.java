package com.java.agilian.rabbit.mq.service;

/**
 * Service class that calls Provider Memo Alert Rest Api's
 *
 * @author Buddha.
 * @Date 6/28/2022.
 */
public interface ProviderMemoAlertService {

    public String getProviderMemos(final String providerId) throws Exception;

    public String updateProviderMemos(final String provider) throws Exception;

    public String getProviderAlerts(final String providerId) throws Exception;

    public String updateProviderAlerts(final String provider) throws Exception;
}
