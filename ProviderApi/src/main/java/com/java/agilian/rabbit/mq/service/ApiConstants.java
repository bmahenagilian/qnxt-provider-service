package com.java.agilian.rabbit.mq.service;

/**
 * Api Constants.
 *
 * @author Buddha.
 * @Date 6/28/2022.
 */
public interface ApiConstants {

    // Http Entity & Header Values.
    String ACCESS_TOKEN = "access_token";
    String API_HEADER_NAME = "X-TZ-EnvId";
    String API_HEADER_VALUE = "1";
    String AUTHORIZATION = "Authorization";
    String TOKEN_URL = "https://b63qnxthubreg3.cishoc.com:12808/QNXTsts";


    // provider api urls
    String PROVIDERS_SEARCH = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/search";
    String GET_PROVIDER = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S";
    String SEARCH_PAY_TO_AFFILIATES = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/affiliations/search";
    String SERVICE_LOCATIONS_SEARCH = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/serviceLocations/search";
    String GET_PROVIDER_SPECIALITY_GROUPS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/specialtyGroups";
    String GET_PROVIDER_SUMMARY_BY_PROVIDER_IDS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/get";
    String GET_PROVIDER_NETWORK_PAR_STATUS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/networkParStatuses/check";


    // provider memo alert api urls
    String GET_PROVIDER_MEMOS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/memoAlerts/memos";
    String UPDATE_PROVIDER_MEMOS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/memoAlerts/memos";
    String GET_PROVIDER_ALERTS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/memoAlerts/alerts";
    String UPDATE_PROVIDER_ALERTS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/memoAlerts/alerts";
}
