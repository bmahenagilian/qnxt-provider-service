package com.java.agilian.rabbit.mq.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProvidersResult {
    private long totalCount;
    private long take;
    private long skip;
    private List<Provider> results;
    private ProcessMetadata processMetadata;

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long value) {
        this.totalCount = value;
    }

    public long getTake() {
        return take;
    }

    public void setTake(long value) {
        this.take = value;
    }

    public long getSkip() {
        return skip;
    }

    public void setSkip(long value) {
        this.skip = value;
    }

    public List<Provider> getResults() {
        return results;
    }

    public void setResults(List<Provider> results) {
        this.results = results;
    }

    public ProcessMetadata getProcessMetadata() {
        return processMetadata;
    }

    public void setProcessMetadata(ProcessMetadata value) {
        this.processMetadata = value;
    }
}