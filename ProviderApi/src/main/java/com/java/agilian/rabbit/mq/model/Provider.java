package com.java.agilian.rabbit.mq.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;

/**
 * @author Buddha.
 * @Date 6/21/2022.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Provider {

    private String provID;
    private String name;
    private String entityID;
    private String npi;
    private String fedID;
    private String status;
    private String credentialStatus;
    private String distance;
    private String phyAddress1;
    private String phyCity;
    private String phyCountyCode;
    private String phyState;
    private String phyZipCode;
    private String address1;
    private String city;
    private String countyCode;
    private String state;
    private String zipCode;
    private String primaryPhone;
    private String emergencyPhone;
    private String programID;
    private boolean participating;
    private String networkID;
    private boolean inNetwork;
    private String upin;
    private String taxonomyCode;
    private String externalProviderID;
    private Timestamp birthDate;
    private String entityState;

    public String getProvID() {
        return provID;
    }

    public void setProvID(String provID) {
        this.provID = provID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEntityID() {
        return entityID;
    }

    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    public String getNpi() {
        return npi;
    }

    public void setNpi(String npi) {
        this.npi = npi;
    }

    public String getFedID() {
        return fedID;
    }

    public void setFedID(String fedID) {
        this.fedID = fedID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCredentialStatus() {
        return credentialStatus;
    }

    public void setCredentialStatus(String credentialStatus) {
        this.credentialStatus = credentialStatus;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPhyAddress1() {
        return phyAddress1;
    }

    public void setPhyAddress1(String phyAddress1) {
        this.phyAddress1 = phyAddress1;
    }

    public String getPhyCity() {
        return phyCity;
    }

    public void setPhyCity(String phyCity) {
        this.phyCity = phyCity;
    }

    public String getPhyCountyCode() {
        return phyCountyCode;
    }

    public void setPhyCountyCode(String phyCountyCode) {
        this.phyCountyCode = phyCountyCode;
    }

    public String getPhyState() {
        return phyState;
    }

    public void setPhyState(String phyState) {
        this.phyState = phyState;
    }

    public String getPhyZipCode() {
        return phyZipCode;
    }

    public void setPhyZipCode(String phyZipCode) {
        this.phyZipCode = phyZipCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getEmergencyPhone() {
        return emergencyPhone;
    }

    public void setEmergencyPhone(String emergencyPhone) {
        this.emergencyPhone = emergencyPhone;
    }

    public String getProgramID() {
        return programID;
    }

    public void setProgramID(String programID) {
        this.programID = programID;
    }

    public boolean isParticipating() {
        return participating;
    }

    public void setParticipating(boolean participating) {
        this.participating = participating;
    }

    public String getNetworkID() {
        return networkID;
    }

    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    public boolean isInNetwork() {
        return inNetwork;
    }

    public void setInNetwork(boolean inNetwork) {
        this.inNetwork = inNetwork;
    }

    public String getUpin() {
        return upin;
    }

    public void setUpin(String upin) {
        this.upin = upin;
    }

    public String getTaxonomyCode() {
        return taxonomyCode;
    }

    public void setTaxonomyCode(String taxonomyCode) {
        this.taxonomyCode = taxonomyCode;
    }

    public String getExternalProviderID() {
        return externalProviderID;
    }

    public void setExternalProviderID(String externalProviderID) {
        this.externalProviderID = externalProviderID;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getEntityState() {
        return entityState;
    }

    public void setEntityState(String entityState) {
        this.entityState = entityState;
    }
}
