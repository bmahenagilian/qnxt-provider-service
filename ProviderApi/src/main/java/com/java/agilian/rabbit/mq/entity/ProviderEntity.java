package com.java.agilian.rabbit.mq.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Provider Entity that stores provider information.
 */
@Entity
@Table(name = "provider_mom")
public class ProviderEntity {

    @Id
    @Column(name = "id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
