package com.java.agilian.rabbit.mq.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static com.java.agilian.rabbit.mq.util.HeadersUtils.getBasicAuthenticationHeader;
import static com.java.agilian.rabbit.mq.util.HeadersUtils.getBearerHeader;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @author Buddha.
 * @Date 6/23/2022.
 */
@Service
public class ProviderRestServiceImpl {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String API_HEADER_NAME = "X-TZ-EnvId";
    public static final String API_HEADER_VALUE = "1";
    private static final Logger log = LoggerFactory.getLogger(QnxtProducerServiceImpl.class);

    private static final String TOKEN_URL = "https://b63qnxthubreg3.cishoc.com:12808/QNXTsts";

    private static final String PROVIDERS_SEARCH = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/search";

    private static final String GET_PROVIDER = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S";

    private static final String SEARCH_PAY_TO_AFFILIATES = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/affiliations/search";

    private static final String SERVICE_LOCATIONS_SEARCH = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Provider/providers/%S/serviceLocations/search";
    private RestTemplate restTemplate = new RestTemplate();

    public String getProviderById(final String providerId) throws Exception {

        final HttpEntity httpEntity = getHttpEntity();

        if (httpEntity == null) {
            return null;
        }

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(GET_PROVIDER, providerId), HttpMethod.GET, httpEntity, String.class);
        log.info("Fetched Provider Data for given provider id : " + providerId);

        final String responseBody = apiResponse.getBody();

        return responseBody;
    }

    public String getProvidersSearch() throws Exception {

        final HttpEntity httpEntity = getHttpEntity();

        if (httpEntity == null) {
            return null;
        }

        final ResponseEntity<String> apiResponse = restTemplate.exchange(PROVIDERS_SEARCH, HttpMethod.GET, httpEntity, String.class);

        final String responseBody = apiResponse.getBody();

        return responseBody;
    }

    public String getSearchPayToAffiliates(final String providerId) throws Exception {

        final HttpEntity httpEntity = getHttpEntity();

        if (httpEntity == null) {
            return null;
        }

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(SEARCH_PAY_TO_AFFILIATES, providerId), HttpMethod.GET, httpEntity, String.class);

        final String responseBody = apiResponse.getBody();

        return responseBody;
    }

    public String getServiceLocationsSearch(final String providerId) throws Exception {

        final HttpEntity httpEntity = getHttpEntity();

        if (httpEntity == null) {
            return null;
        }

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(SERVICE_LOCATIONS_SEARCH, providerId), HttpMethod.GET, httpEntity, String.class);

        final String responseBody = apiResponse.getBody();

        return responseBody;
    }

    /**
     * Prepares the http entity.
     *
     * @return http entity.
     * @throws Exception exception.
     */
    private HttpEntity getHttpEntity() throws Exception {

        //creating headers
        final HttpHeaders headers = getBasicAuthenticationHeader("APIServAcct", "");
        headers.add(API_HEADER_NAME, API_HEADER_VALUE);

        //creating http entity
        final HttpEntity requestEntity = new HttpEntity(headers);

        // making api call
        final ResponseEntity<String> response = restTemplate.exchange(TOKEN_URL, HttpMethod.GET, requestEntity, String.class);

        if (isBlank(response.getBody())) {
            log.info("No response from access token api.");
            return null;
        }

        // converting response string to map.
        final Map<String, Object> responseMap = new ObjectMapper().readValue(response.getBody(), HashMap.class);

        if (!responseMap.containsKey(ACCESS_TOKEN)) {
            log.info("Response not contains access token..");
            return null;
        }

        final String accessToken = (String) responseMap.get(ACCESS_TOKEN);
        final HttpHeaders apiHeaders = getBearerHeader(accessToken);
        apiHeaders.add(API_HEADER_NAME, API_HEADER_VALUE);

        return new HttpEntity(apiHeaders);
    }
}
