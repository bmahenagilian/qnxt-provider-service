package com.java.agilian.rabbit.mq.service;

import java.util.List;

/**
 * Service class that calls provider api's
 *
 * @author Buddha.
 * @Date 6/28/2022.
 */
public interface ProviderService {

    /**
     * Get provider by provider id.
     *
     * @param providerId provider id.
     * @return provider info.
     * @throws Exception exception.
     */
    String getProviderById(final String providerId) throws Exception;

    /**
     * Get all providers.
     *
     * @return providers.
     * @throws Exception exception.
     */
    String getProvidersSearch() throws Exception;

    /**
     * Get search pay to affiliate by id.
     *
     * @param providerId provider id.
     * @return pay to affiliates.
     * @throws Exception
     */
    String getSearchPayToAffiliatesById(final String providerId) throws Exception;

    /**
     * Gets service locations by provider id.
     *
     * @param providerId provider id.
     * @return returns service locations.
     * @throws Exception exception.
     */
    String getServiceLocationsById(final String providerId) throws Exception;

    /**
     * Gets speciality groups by id.
     *
     * @param providerId provider id.
     * @return speciality groups.
     * @throws Exception exception.
     */
    String getProviderSpecialityGroupsById(final String providerId) throws Exception;

    /**
     * Gets summary by provider ids.
     *
     * @param providerIds provider ids.
     * @return provider summaries.
     * @throws Exception
     */
    String getProviderSummaryByProviderIds(final List<String> providerIds) throws Exception;

    /**
     * Gets the network par statuses by provider id.
     *
     * @param providerId provider id.
     * @return returns response.
     * @throws Exception exception.
     */
    String getProviderNetworkParStatusById(final String providerId) throws Exception;
}
