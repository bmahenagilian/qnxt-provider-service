package com.java.agilian.rabbit.mq.service.impl;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

@Service
public class QnxtProducerServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(QnxtProducerServiceImpl.class);
    @Value("${spring.rabbitmq.username}")
    private String username;
    @Value("${spring.rabbitmq.password}")
    private String pwd;
    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.exchange}")
    private String exchange;
    @Value("${spring.rabbitmq.port}")
    private Integer port;
    @Value("${spring.rabbitmq.routingkey}")
    private String routingkey;

    private ConnectionFactory factory = new ConnectionFactory();

    public void send(final String message) throws NoSuchAlgorithmException, KeyManagementException, IOException, TimeoutException {

        factory.setUsername(username);
        factory.setPassword(pwd);

        factory.setHost(host);
        factory.setPort(port);
        // Allows client to establish a connection over TLS
        factory.useSslProtocol("TLSv1.2");

        // Create a connection
        final Connection conn = factory.newConnection();

        // Create a channel
        final Channel channel = conn.createChannel();
        final byte[] messageBodyBytes = message.toString().getBytes();
        channel.basicPublish(exchange, routingkey, new AMQP.BasicProperties.Builder().contentType("text/plain").userId(username).build(), message.getBytes());

        log.info("Send msg = " + message);
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setRoutingkey(String routingkey) {
        this.routingkey = routingkey;
    }
}