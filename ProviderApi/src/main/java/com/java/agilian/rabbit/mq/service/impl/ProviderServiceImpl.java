package com.java.agilian.rabbit.mq.service.impl;

import com.java.agilian.rabbit.mq.service.ProviderService;
import com.java.agilian.rabbit.mq.util.HeadersUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

import static com.java.agilian.rabbit.mq.service.ApiConstants.*;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Implementation of {@link ProviderService}
 *
 * @author Buddha.
 * @Date 6/23/2022.
 */
@Service
public class ProviderServiceImpl implements ProviderService {

    private static final Logger log = LoggerFactory.getLogger(QnxtProducerServiceImpl.class);

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private QnxtProducerServiceImpl producerService;

    @Override
    public String getProviderById(final String providerId) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);
        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(GET_PROVIDER, providerId), HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String getProvidersSearch() throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);
        final ResponseEntity<String> apiResponse = restTemplate.exchange(PROVIDERS_SEARCH, HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String getSearchPayToAffiliatesById(final String providerId) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(SEARCH_PAY_TO_AFFILIATES, providerId), HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String getServiceLocationsById(final String providerId) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(SERVICE_LOCATIONS_SEARCH, providerId), HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String getProviderSpecialityGroupsById(final String providerId) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(GET_PROVIDER_SPECIALITY_GROUPS, providerId), HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String getProviderSummaryByProviderIds(final List<String> providerIds) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final String url = UriComponentsBuilder.fromHttpUrl(GET_PROVIDER_SUMMARY_BY_PROVIDER_IDS).queryParam("provIds", providerIds).toUriString();

        final ResponseEntity<String> apiResponse = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String getProviderNetworkParStatusById(final String providerId) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(GET_PROVIDER_NETWORK_PAR_STATUS, providerId), HttpMethod.GET, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }
}
