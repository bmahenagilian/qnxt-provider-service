package com.java.agilian.rabbit.mq.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessMetadata {
    private String conversationID;
    private String transactionID;

    private List<ProcessMessage> processMessages;

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String value) {
        this.conversationID = value;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    public List<ProcessMessage> getProcessMessages() {
        return processMessages;
    }

    public void setProcessMessages(List<ProcessMessage> processMessages) {
        this.processMessages = processMessages;
    }
}