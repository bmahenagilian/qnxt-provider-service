package com.java.agilian.rabbit.mq.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdditionalInfo {

    private AdditionalProperties additionalProp1;
    private AdditionalProperties additionalProp2;
    private AdditionalProperties additionalProp3;

    public AdditionalProperties getAdditionalProp1() {
        return additionalProp1;
    }

    public void setAdditionalProp1(AdditionalProperties additionalProp1) {
        this.additionalProp1 = additionalProp1;
    }

    public AdditionalProperties getAdditionalProp2() {
        return additionalProp2;
    }

    public void setAdditionalProp2(AdditionalProperties additionalProp2) {
        this.additionalProp2 = additionalProp2;
    }

    public AdditionalProperties getAdditionalProp3() {
        return additionalProp3;
    }

    public void setAdditionalProp3(AdditionalProperties additionalProp3) {
        this.additionalProp3 = additionalProp3;
    }
}