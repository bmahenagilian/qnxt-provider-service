package com.java.agilian.rabbit.mq.service.impl;

import com.java.agilian.rabbit.mq.service.ProviderMemoAlertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service class that performs api calls on Provider Memo Alerts.
 *
 * @author Buddha.
 * @Date 6/28/2022.
 */
@Service
public class ProviderMemoAlertServiceImpl implements ProviderMemoAlertService {

    private static final Logger log = LoggerFactory.getLogger(ProviderMemoAlertServiceImpl.class);

    private final RestTemplate restTemplate = new RestTemplate();


    @Override
    public String getProviderMemos(final String providerId) throws Exception {
        return null;
    }

    @Override
    public String updateProviderMemos(final String provider) throws Exception {
        return null;
    }

    @Override
    public String getProviderAlerts(final String providerId) throws Exception {
        return null;
    }

    @Override
    public String updateProviderAlerts(final String provider) throws Exception {
        return null;
    }
}
