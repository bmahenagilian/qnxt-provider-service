package com.java.agilian.rabbit.mq.controller;

import com.java.agilian.rabbit.mq.service.ProviderService;
import com.java.agilian.rabbit.mq.service.impl.QnxtConsumerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "provider", produces = "application/json")
public class ProviderApiController {

    @Autowired
    ProviderService providerService;

    @Autowired
    QnxtConsumerServiceImpl consumerService;

    @GetMapping(value = "get-provider")
    public ResponseEntity<String> getProvider(final HttpServletRequest request, @RequestParam("providerId") String providerId) throws Exception {

        final String response = providerService.getProviderById(providerId);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "search-providers")
    public ResponseEntity<String> providerSearch(final HttpServletRequest request) throws Exception {

        final String response = providerService.getProvidersSearch();

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "affiliation-search/{provider-id}")
    public ResponseEntity<String> affiliationSearchByProviderId(final HttpServletRequest request, @PathVariable(name = "provider-id") String providerId) throws Exception {

        final String response = providerService.getSearchPayToAffiliatesById(providerId);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "service-locations/{provider-id}")
    public ResponseEntity<String> serviceLocationsByProviderId(final HttpServletRequest request, @PathVariable(name = "provider-id") String providerId) throws Exception {

        final String response = providerService.getServiceLocationsById(providerId);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "specialty-groups/{provider-id}")
    public ResponseEntity<String> getSpecialtyGroupsByProviderId(final HttpServletRequest request, @PathVariable(name = "provider-id") String providerId) throws Exception {

        final String response = providerService.getProviderSpecialityGroupsById(providerId);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "providers-summary")
    public ResponseEntity<String> getProviderSummaryByProviderIds(final HttpServletRequest request, @RequestParam(name = "provIds") List<String> providerIds) throws Exception {

        final String response = providerService.getProviderSummaryByProviderIds(providerIds);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "network-par-statuses/{provider-id}")
    public ResponseEntity<String> getProviderNetworkParStatuses(final HttpServletRequest request, @PathVariable(name = "provider-id") String providerId) throws Exception {

        final String response = providerService.getProviderNetworkParStatusById(providerId);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "consumer")
    public ResponseEntity<String> consumer() throws Exception {

        final String response = consumerService.receive();

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }
}

