package com.java.agilian.rabbit.mq.repository;

import com.java.agilian.rabbit.mq.entity.ProviderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Provider entity to store Provider api response.
 *
 * @author Buddha.
 * @Date 6/21/2022.
 */
@Repository
public interface ProviderRepository extends JpaRepository<ProviderEntity, Long> {


}
